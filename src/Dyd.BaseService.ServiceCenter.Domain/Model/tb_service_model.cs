﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_service
    public class tb_service
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 服务名称
        /// </summary>
        [Display(Name = "服务名称")]
        public string servicename { get; set; }
        /// <summary>
        /// 服务命名空间（服务唯一标示）
        /// </summary>
        [Display(Name = "服务命名空间")]
        public string servicenamespace { get; set; }
        /// <summary>
        /// 服务类型, 1=thrift,2=wcf,3=http
        /// </summary>
        [Display(Name = "类型")]
        public int servicetype { get; set; }
        /// <summary>
        /// 是否测试
        /// </summary>
        [Display(Name = "测试")]
        public bool istest { get; set; }
        /// <summary>
        /// 是否自动设置服务权重（根据服务性能，ping响应速度）
        /// </summary>
        [Display(Name = "自动设置权重")]
        public bool isautoboostpercent { get; set; }
        /// <summary>
        /// 协议版本号（当前）
        /// </summary>
        [Display(Name = "当前协议版本")]
        public int protocolversion { get; set; }
        /// <summary>
        /// 协议更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        public DateTime protocolversionupdatetime { get; set; }
        /// <summary>
        /// 服务变动更新时间（服务节点移除，添加需要更新这个时间）
        /// </summary>
        [Display(Name = "变动时间")]
        public DateTime serviceupdatetime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        public string remark { get; set; }

        /// <summary>
        /// 停止节点数
        /// </summary>
        public int stopnodecount { get; set; }

        /// <summary>
        /// 运行节点数
        /// </summary>
        public int runningnodecount { get; set; }

    }

    //tb_service
    public class tb_service_search : BaseSearch
    {
        public tb_service_search()
        {
            this.servicetype = int.MinValue;
        }
        /// <summary>
        /// key
        /// </summary>
        [Display(Name = "key")]
        public string key { get; set; }

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// 服务名称
        /// </summary>
        [Display(Name = "服务名称")]
        public string servicename { get; set; }
        /// <summary>
        /// 服务命名空间（服务唯一标示）
        /// </summary>
        [Display(Name = "服务命名空间（服务唯一标示）")]
        public string servicenamespace { get; set; }
        /// <summary>
        /// 服务类型, 1=thrift,2=wcf,3=http
        /// </summary>
        [Display(Name = "服务类型, 1=thrift,2=wcf,3=http")]
        public int servicetype { get; set; }
        /// <summary>
        /// 是否测试
        /// </summary>
        [Display(Name = "是否测试")]
        public bool istest { get; set; }
        /// <summary>
        /// 是否自动设置服务权重（根据服务性能，ping响应速度）
        /// </summary>
        [Display(Name = "是否自动设置服务权重（根据服务性能，ping响应速度）")]
        public bool isautoboostpercent { get; set; }
        /// <summary>
        /// 协议版本号（当前）
        /// </summary>
        [Display(Name = "协议版本号（当前）")]
        public decimal protocolversion { get; set; }
        /// <summary>
        /// 协议更新时间
        /// </summary>
        [Display(Name = "协议更新时间")]
        public DateTime protocolversionupdatetime { get; set; }
        /// <summary>
        /// 服务变动更新时间（服务节点移除，添加需要更新这个时间）
        /// </summary>
        [Display(Name = "服务变动更新时间（服务节点移除，添加需要更新这个时间）")]
        public DateTime serviceupdatetime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        public string remark { get; set; }

    }
}