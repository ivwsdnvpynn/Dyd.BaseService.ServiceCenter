﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ServiceCenter.Domain.Model
{
    //tb_system_config
    public class tb_system_config
    {

        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// key
        /// </summary>
        [Display(Name = "键")]
        [Required]
        public string key { get; set; }
        /// <summary>
        /// value
        /// </summary>
        [Required]
        [Display(Name = "值")]
        public string value { get; set; }
        /// <summary>
        /// remark
        /// </summary>
        [Display(Name = "备注")]
        public string remark { get; set; }

    }

    //tb_system_config
    public class tb_system_config_Search : BaseSearch
    {
        /// <summary>
        /// id
        /// </summary>
        [Display(Name = "id")]
        public int id { get; set; }
        /// <summary>
        /// key
        /// </summary>
        [Display(Name = "key")]
        public string key { get; set; }
        /// <summary>
        /// value
        /// </summary>
        [Display(Name = "value")]
        public string value { get; set; }
        /// <summary>
        /// remark
        /// </summary>
        [Display(Name = "remark")]
        public string remark { get; set; }

    }
}