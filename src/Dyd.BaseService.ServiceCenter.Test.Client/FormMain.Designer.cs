﻿namespace Dyd.BaseService.ServiceCenter.Test.Client
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCall = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnMutiCall = new System.Windows.Forms.Button();
            this.textBoxCallCount = new System.Windows.Forms.TextBox();
            this.textBoxCallThread = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCall
            // 
            this.btnCall.Location = new System.Drawing.Point(124, 12);
            this.btnCall.Name = "btnCall";
            this.btnCall.Size = new System.Drawing.Size(75, 23);
            this.btnCall.TabIndex = 0;
            this.btnCall.Text = "调用";
            this.btnCall.UseVisualStyleBackColor = true;
            this.btnCall.Click += new System.EventHandler(this.btnCall_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(31, 41);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(546, 192);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // btnMutiCall
            // 
            this.btnMutiCall.Location = new System.Drawing.Point(230, 12);
            this.btnMutiCall.Name = "btnMutiCall";
            this.btnMutiCall.Size = new System.Drawing.Size(75, 23);
            this.btnMutiCall.TabIndex = 2;
            this.btnMutiCall.Text = "调用性能";
            this.btnMutiCall.UseVisualStyleBackColor = true;
            this.btnMutiCall.Click += new System.EventHandler(this.btnMutiCall_Click);
            // 
            // textBoxCallCount
            // 
            this.textBoxCallCount.Location = new System.Drawing.Point(339, 13);
            this.textBoxCallCount.Name = "textBoxCallCount";
            this.textBoxCallCount.Size = new System.Drawing.Size(100, 21);
            this.textBoxCallCount.TabIndex = 3;
            this.textBoxCallCount.Text = "200000";
            // 
            // textBoxCallThread
            // 
            this.textBoxCallThread.Location = new System.Drawing.Point(464, 13);
            this.textBoxCallThread.Name = "textBoxCallThread";
            this.textBoxCallThread.Size = new System.Drawing.Size(100, 21);
            this.textBoxCallThread.TabIndex = 4;
            this.textBoxCallThread.Text = "50";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(31, 12);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "清除";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(612, 277);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.textBoxCallThread);
            this.Controls.Add(this.textBoxCallCount);
            this.Controls.Add(this.btnMutiCall);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnCall);
            this.Name = "FormMain";
            this.Text = "测试客户端";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCall;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btnMutiCall;
        private System.Windows.Forms.TextBox textBoxCallCount;
        private System.Windows.Forms.TextBox textBoxCallThread;
        private System.Windows.Forms.Button btnClear;
    }
}

