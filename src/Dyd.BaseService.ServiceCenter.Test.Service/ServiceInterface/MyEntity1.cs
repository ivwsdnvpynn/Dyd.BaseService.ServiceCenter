﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    [XXF.BaseService.ServiceCenter.Service.OpenDoc.EntityDoc("测试实体", "我的测试实体")]
    public class MyEntity1
    {
        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("实体参数1", "我的测试实体参数1")]
        public int p1 { get; set; }
    }

    [XXF.BaseService.ServiceCenter.Service.OpenDoc.EntityDoc("测试实体2", "我的测试实体2")]
    public class MyEntity2
    {
        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("List<int>", "List<int>参数")]
        public List<int> P2 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("List<byte>", "List<byte>参数")]
        public List<byte> P3 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("Dictionary<string,string>", "Dictionary<string,string>参数")]
        public Dictionary<string,string> P4 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("bool", "bool参数")]
        public bool P5 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("long", "long参数")]
        public long P6 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("short", "short参数")]
        public short P7 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("Int16", "Int16参数")]
        public Int16 P8 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("byte[]", "byte[]参数")]
        public byte[] P9 { get; set; }

        [XXF.BaseService.ServiceCenter.Service.OpenDoc.PropertyDoc("String", "String参数")]
        public String P10 { get; set; }
    }
}
