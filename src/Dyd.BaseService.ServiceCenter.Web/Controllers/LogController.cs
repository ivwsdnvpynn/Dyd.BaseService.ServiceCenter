﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using XXF.Db;
using System.Linq;

namespace Dyd.BaseService.ServiceCenter.Web.Controllers
{
    [Authorize]
    public class LogController : Controller
    {

        #region List
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search">查询实体</param>
        /// <param name="pno">页码</param>
        /// <param name="pagesize">页大小</param>
        /// <returns></returns>
        public ActionResult LogIndex(tb_log_search search)
        {
            int total = 0;
            IList<tb_log> list = new List<tb_log>();
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                #region 服务列表
                IList<tb_service> serviceList = new List<tb_service>();
                serviceList = tb_service_bll.Instance.GetPageList(conn, new tb_service_search(), out total);
                IList<SelectListItem> serviceSelectList = new List<SelectListItem>();
                serviceList.ToList().ForEach((s) =>
                {
                    serviceSelectList.Add(new SelectListItem { Text = s.servicename, Value = s.id.ToString() });
                });
                serviceSelectList.Insert(0, new SelectListItem { Text = "全部", Value = "0" });

                if (Request.IsAjaxRequest() && search.serviceid > 0)
                {
                    var seletedItem = serviceSelectList.FirstOrDefault(s => s.Value == search.serviceid.ToString());
                    if (null != seletedItem)
                    {
                        seletedItem.Selected = true;
                    }
                }
                ViewBag.ServiceList = serviceSelectList;
                #endregion
                list = tb_log_bll.Instance.GetPageList(conn, search, out total);
                var pagelist = new SPagedList<tb_log>(list, search.Pno, search.PageSize, total);

                if (Request.IsAjaxRequest())
                {
                    return PartialView("_LogIndex", pagelist);
                }
                else
                {
                    return View(pagelist);
                }
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogDelete(int id)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    tb_log_bll.Instance.Delete(conn, id);
                    return Json(new { Flag = true, Message = "删除成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }
        #endregion

        #region 清理日志
        [HttpPost]
        public JsonResult LogClear()
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                tb_log_bll.Instance.Clear(conn);
                tb_log_bll.Instance.Add(conn, string.Format("{0}", "清理日志"));
                return Json(new OperateResults { Flag = true, Message = "清理成功！" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}