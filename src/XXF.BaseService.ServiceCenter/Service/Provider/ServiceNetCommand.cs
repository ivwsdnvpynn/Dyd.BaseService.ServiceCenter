﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Service.Provider
{
    /// <summary>
    /// 服务网络命令
    /// </summary>
    public class ServiceNetCommand
    {
        public EnumServiceCommandType CommandType { get; set; }
        public string ServiceNameSpace { get; set; }

        public int ServiceID { get; set; }
        public int NodeId { get; set; }
    }

    /// <summary>
    /// 网络命令类型
    /// </summary>
    public enum EnumServiceCommandType
    {
        /// <summary>
        /// 服务更新
        /// </summary>
        ServiceUpdate,
        /// <summary>
        /// 服务重启
        /// </summary>
        ServiceRestart
    }
}
