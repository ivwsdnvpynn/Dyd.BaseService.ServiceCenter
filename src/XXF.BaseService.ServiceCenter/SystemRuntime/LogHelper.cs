﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Dal;
using XXF.ProjectTool;
using XXF.Extensions;

namespace XXF.BaseService.ServiceCenter.SystemRuntime
{
    public class LogHelper
    {
        public static void Log(int serviceid, EnumLogType logtype, string msg)
        {
            try
            {
               
                Debug(serviceid, logtype, msg);
                if (!string.IsNullOrWhiteSpace(SystemConfigHelper.ServiceCenterConnectString))
                {
                    SqlHelper.ExcuteSql(SystemConfigHelper.ServiceCenterConnectString, (c) =>
                    {
                        tb_log_dal errordal = new tb_log_dal();
                        errordal.Add(c, new Model.tb_log_model() { logtype = (byte)logtype, msg = msg, serviceid = serviceid });
                    });
                }
            }
            catch (Exception exp)
            {
                string str = string.Format("服务中心写日志出错,serviceid:{0},logtype:{1},msg:{2}", serviceid, logtype.ToString(), msg);
                XXF.Log.ErrorLog.Write(str, exp);
            }
        }

        public static void Debug(int serviceid, EnumLogType logtype, string msg)
        {
            string str = string.Format("调试:serviceid:{0},logtype:{1},msg:{2}", serviceid, logtype.ToString(), msg);
            System.Diagnostics.Debug.WriteLine(str);
        }

        public static void Error(int serviceid, EnumLogType logtype, string msg, Exception exception)
        {
            try
            {
                msg = msg.NullToEmpty() + "[error:]" + ((exception != null && exception.Message != null) ? exception.Message.NullToEmpty() : "");
                Debug(serviceid, logtype, msg);
                if (!string.IsNullOrWhiteSpace(SystemConfigHelper.ServiceCenterConnectString))
                {
                    SqlHelper.ExcuteSql(SystemConfigHelper.ServiceCenterConnectString, (c) =>
                    {
                        tb_error_dal errordal = new tb_error_dal();
                        errordal.Add(c, new Model.tb_error_model() { logtype = (byte)logtype, msg = msg, serviceid = serviceid });
                    });
                }
            }
            catch (Exception exp)
            {
                string str = string.Format("服务中心写错误日志出错,serviceid:{0},logtype:{1},msg:{2}", serviceid, logtype.ToString(), msg);
                XXF.Log.ErrorLog.Write(str, exp);
            }
        }
    }
}
